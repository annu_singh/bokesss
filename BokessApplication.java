package com.bookstore;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@ComponentScan
@SpringBootApplication
public class BokessApplication {

	public static void main(String[] args) {
		SpringApplication.run(BokessApplication.class, args);
		
		   System.out.println("App started....");
	}

}
